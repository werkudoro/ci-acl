'use strict';

/**
 * Delete record
 * @param  {string} formId Form ID
 * @return {vooid}
 */
function doDelete(url) {
    var confirmDelete = bootbox.confirm('Anda yakin ingin menghapus record ini?', function (result) {
        if (result)
        {
            location.href=url;
        }
    });
}

/**
 * AJAX Checkbox
 * @param  {object} objChecklist Checklist object
 * @param  {string} url          URL
 * @return {void}
 */
function ajaxCheckbox(objChecklist, url) {
    objChecklist.parent().append('<i data-role="spinner" class="glyphicon glyphicon-repeat fast-right-spinner" style="display:none;"></i>');
    var $spinner = objChecklist.parent().find('[data-role="spinner"]');
    var isChecked = objChecklist.is(":checked");

    $.ajax({
        url: url,
        beforeSend: function (xhr) {
            objChecklist.hide();
            $spinner.show();
        },
        success: function (data) {
            if (data === 'OK OFF')
                objChecklist.prop("checked", false);
            else
                objChecklist.prop("checked", true);

            objChecklist.show();
            $spinner.remove();
        },
        error: function () {
            objChecklist.prop("checked", ! isChecked);
            objChecklist.show();
            $spinner.remove();
        }
    });
}

$(document).ready(function () {

    $('table[data-role="datatable"]').each(function () {
        var $tbl = $(this);
        var cols = [];

        $tbl.find('thead th').each(function () {
            if ($(this).hasClass('fit')) {
                cols.push({className: 'fit'});
            } else {
                cols.push(null);
            }
        });

        $tbl.DataTable({
            autoWidth: true,
            searchDelay: 2000,
            stateSave: true,
            processing: true,
            serverSide: true,
            columns: cols,
            ajax: {
                url: $(this).data('url'),
                type: 'GET'
            },
        });
    });

    $('[data-role="ajax-checkbox"]').each(function () {
        $(this).click(function () {
            ajaxCheckbox($(this), $(this).data('url'));
        });
    });

});
