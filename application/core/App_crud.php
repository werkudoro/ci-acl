<?php

defined('BASEPATH') or exit('No direct script access allowed');


class App_crud extends App
{
    /**
     * @var integer
     */
    protected $per_page = 10;

    /**
     * Data table
     *
     * @return array
     */
    protected function parse_datatable_params()
    {
        $draw = (int) $this->input->get('draw');
        $start = (int) $this->input->get('start');
        $length = (int) $this->input->get('length');
        $length = $length ? $length : $this->per_page;
        $search = $this->input->get('search');
        $order = $this->input->get('order');

        $keywords = '';
        if (isset($search['value']))
        {
            $keywords = str_replace(' ', '%' , addslashes($search['value']));
        }

        $col_order = 0;

        if (isset($order[0]['column']))
        {
            $col_order = (int) $order[0]['column'];
        }

        $dir_order = isset($order[0]['dir']) && $order[0]['dir'] === 'asc' ? 'asc' : 'desc';

        return [
            'draw' => $draw,
            'start' => $start,
            'length' => $length,
            'search' => $search,
            'order' => $order,
            'col_order' => $col_order,
            'dir_order' => $dir_order,
            'keywords' => $keywords
        ];
    }

    /**
     * Data Table Action Column
     *
     * @param object $obj
     * @param string $template_path
     * @return string
     */
    protected function action_column($obj, $template_path)
    {
        return $this->load->view($template_path, ['obj' => $obj], TRUE);
    }

    /**
     * @return void
     */
    protected function datatable_response(array $data, $num_total, $num_filtered, $draw)
    {
        header("Content-type:application/json");
        echo json_encode([
            "draw" => $draw,
            "recordsTotal" => $num_total,
            "recordsFiltered" => $num_filtered,
            "data" => $data
        ]);
        exit(0);
    }

    /**
     * Get 1 record or 404
     *
     * @param  string $model_name Model Name
     * @param  string $id ID
     * @return object
     */
    protected function first_or_404($model_name, $id)
    {
        $this->load->model($model_name);

        $m = explode('/', $model_name);
        $model = $m[count($m) - 1];

        $obj = $this->{$model}->first($id);
        if ( ! $obj)
        {
            show_404(); exit();
        }
        return $obj;
    }
}
