<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Base_controller extends CI_Controller
{
    /**
     * @var string
     */
    const SESS_USER = 'app_user';

    /**
     * @var string
     */
    const SESS_FLASH = 'app_flash';

    /**
     * @var string
     */
    const SESS_FLASH_SEPARATOR = '::::::::::';

    /**
     * @var string
     */
    const MSG_ERROR = 'error';

    /**
     * @var string
     */
    const MSG_SUCCESS = 'success';

    /**
     * @var string
     */
    const MSG_INFO = 'info';

    /**
     * @var string
     */
    const MSG_WARNING = 'warning';

    /**
     * @var array
     */
    protected $data = [];

    /**
     * @param string $type
     * @return array
     */
    protected function set_msg_type($type)
    {
        $allowed_msg_type = [self::MSG_ERROR, self::MSG_INFO, self::MSG_SUCCESS, self::MSG_WARNING];
        return in_array($type, $allowed_msg_type) ? $type : self::MSG_INFO;
    }

    /**
     * Flash message
     *
     * @param string $message Message string
     * @param string $type Message type. 'info', 'error', 'success', 'warning'
     * @return void
     */
    protected function flash($message, $type='info')
    {
        $this->session->set_flashdata(self::SESS_FLASH, $message.self::SESS_FLASH_SEPARATOR.$this->set_msg_type($type));
    }

    /**
     * Set Message
     *
     * @param string $message Message string
     * @param string $type Message type. 'info', 'error', 'success', 'warning'
     * @return void
     */
    protected function set_message($message, $type='info')
    {
        $this->data['message'] = (object) [
            'message' => $message,
            'type' => $this->set_msg_type($type)
        ];
    }

    /**
     * Extract flash message
     * @return void
     */
    protected function extract_flash()
    {
        $msg = $this->session->flashdata(self::SESS_FLASH);
        if ($msg)
        {
            $msg = explode(self::SESS_FLASH_SEPARATOR, $msg);
            $this->set_message($msg[0], $msg[1]);
        }
    }

    /**
     * Display
     *
     * @param  string $path Path
     * @return void
     */
    public function display($path)
    {
        $this->load->view($path, $this->data);
    }

    public function __construct()
    {
        parent::__construct();
        $this->extract_flash();
    }
}
