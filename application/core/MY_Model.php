<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MY_Model extends CI_Model
{
    /**
     * @var string
     */
    protected $table = null;

    /**
     * @var string
     */
    protected $field_pk = 'id';

    /**
     * @var boolean
     */
    protected $has_softdelete = false;

    /**
     * Insert Record
     *
     * @param array $data
     * @return mixed Record ID
     */
    public function insert(array $data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    /**
     * Update Record
     *
     * @param array $data
     * @param mixed $pk Default null
     * @return boolean
     */
    public function update(array $data, $pk=null)
    {
        if ($pk)
        {
            $this->db->where($this->field_pk, $pk)->limit(1);
        }

        if ($this->has_softdelete)
        {
            $this->db->where('deleted_at', null);
        }

        return $this->db->update($this->table, $data) ? true : false;
    }

    /**
     * Undocumented function
     *
     * @param mixed $pk ID. Default null.
     * @return boolean
     */
    public function delete($pk=null)
    {
        if ($pk)
        {
            $this->db->where($this->field_pk, $pk)->limit(1);
        }

        if ($this->has_softdelete)
        {
            return $this->update(['deleted_at' => date('Y-m-d H:i:s')]);
        }

       return $this->db->delete($this->table) ? true : false;
    }

    /**
     * Create Record
     *
     * @param array $data
     * @return mixed{object|boolean}
     */
    public function create(array $data)
    {
        $pk = $this->insert($data);
        if ($pk)
        {
            $this->db->where($this->field_pk, $pk);
            return $this->first();
        }
        return false;
    }

    /**
     * Get First
     *
     * @param mixed $pk ID. Default null.
     * @return mixed{object|boolean}
     */
    public function first($pk=null)
    {
        if ($pk)
        {
            $this->db->where($this->field_pk, $pk)->limit(1);
        }

        $q = $this->db->limit(1)->get($this->table);
        return $q->num_rows() ? $q->row() : false;
    }

    /**
     * Get All
     *
     * @return array
     */
    public function all()
    {
        $q = $this->db->get($this->table);
        return $q->num_rows() ? $q->result() : [];
    }

    /**
     * Guess table
     *
     * @return void
     */
    private function guess_table()
    {
        if ( ! $this->table)
        {
            $this->table = strtolower(
                    str_replace('_model', '', call_user_func('get_class', $this))
                ).'s';
        }
    }

    public function __construct()
    {
        parent::__construct();
        $this->guess_table();
    }
}
