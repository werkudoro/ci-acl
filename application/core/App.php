<?php

defined('BASEPATH') or exit('No direct script access allowed');


class App extends Base_controller
{
    /**
     * @var object
     */
    protected $user = null;

    /**
     * Get active user
     *
     * @return mixed{object|null}
     */
    protected function get_active_user()
    {
        $user_id = $this->session->userdata(self::SESS_USER);
        if ( ! $user_id)
        {
            return null;
        }
        $q = $this->db->where('is_active', true)->where('id', $user_id)->get('users');
        return $q->num_rows() ? $q->row() : null;
    }

    /**
     * Require Permission
     *
     * @param  string $permission_name Permission Name
     * @return void
     */
    protected function require_permission($permission_name)
    {
        $this->load->library('acl');
        if ( ! $this->acl->has_permission($permission_name, $this->user->id))
        {
            $this->flash('Hak akses Anda tidak mencukupi', 'error');
            redirect('dashboard');
        }
    }

    /**
     * Require Role
     *
     * @param  string $role_name Permission Name
     * @return void
     */
    protected function require_role($role_name)
    {
        $this->load->library('acl');
        if ( ! $this->acl->has_role($role_name, $this->user->id))
        {
            $this->flash('Role Anda tidak mencukupi', 'error');
            redirect('dashboard');
        }
    }

    public function __construct()
    {
        parent::__construct();
        $this->user = $this->get_active_user();

        if ( ! $this->user)
        {
            redirect('acl/auth');
        }

        $this->data['user'] = $this->user;
    }
}
