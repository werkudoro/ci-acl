<?php

defined('BASEPATH') or exit('No direct script access allowed');


class User_model extends MY_Model
{
    /**
     * Get user by email
     * @param  string $email Email
     * @return mixed{object|boolean}
     */
    public function getby_email($email)
    {
        $q = $this->db
            ->where('email', $email)
            ->limit(1)
            ->get($this->table);
        return $q->num_rows() ? $q->row() : false;
    }
}
