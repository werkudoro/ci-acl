<?php

defined('BASEPATH') or exit('No direct script access allowed');


class MY_Form_validation extends CI_Form_validation
{
    /**
     * Valid Name
     *
     * @param  string $val String name
     * @return boolean
     */
    public function human_name($val)
    {
        $rexSafety = "/[\^<,\"@\/\{\}\(\)\*\$%\?=>:\|;#]+/i";
        if (preg_match($rexSafety, $val))
        {
            $this->form_validation->set_message('human_name', 'Format nama salah');
            return false;
        }
        return true;
    }
}
