<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Acl
{
    /**
     * User has permission
     * @param  string  $permission_name Permission's name
     * @param  integer  $user_id        User ID
     * @return boolean
     */
    public function has_permission($permission_name, $user_id)
    {
        $q = $this->db->where('id', $user_id)->limit(1)->get();

        if ( ! $q->num_rows())
        {
            return false;
        }

        $q = $this->db
            ->from('permissions_users')
            ->join('permissions', 'permissions.id=permissions_users.permission_id')
            ->where('permissions_users.user_id', $user_id)
            ->where('permissions.name', $permission_name)
            ->get();

        if ( ! $q->num_rows())
        {
            $q = $this->db->where('user_id', $user_id)->get('roles_users');

            if ( ! $q->num_rows())
            {
                return false;
            }

            $role_ids = [];
            foreach ($q->result() as $r)
            {
                $role_ids[] = $r->role_id;
            }

            $q = $this->db
                ->from('permissions_roles')
                ->join('permissions', 'permissions.id=permissions_roles.permission_id')
                ->where_in('permissions_roles.role_id', $role_ids)
                ->where('permissions.name', $permission_name)
                ->get();

            if ( ! $q->num_rows())
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Has Role
     * @param  string  $role_name Role Name
     * @param  integer  $user_id  ID
     * @return boolean
     */
    public function has_role($role_name, $user_id)
    {
        $q = $this->db
            ->from('roles_users')
            ->join('roles', 'roles.id=roles_users.role_id')
            ->where('roles.name', $role_name)
            ->where('user_id', $user_id)
            ->limit(1)
            ->get()

        return $q->num_rows() ? true : false;
    }
}
