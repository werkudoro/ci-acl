<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Dashboard extends App
{
    public function index()
    {
        $this->display('dashboard/index');
    }

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->user->is_staff)
        {
            redirect('site');
        }
    }
}
