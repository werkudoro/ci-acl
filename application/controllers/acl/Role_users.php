<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Role_users extends App_crud
{
    public function index()
    {
        $id = (int) $this->input->get('id');
        $obj = $this->first_or_404('user_model', $id);
        $q = $this->db->get('roles');
        $roles = [];
        $role_users = [];

        foreach ($q->result() as $r)
        {
            $roles[$r->id] = $r->name;
        }

        $q = $this->db
            ->select('roles_users.id, role_id, roles.name, user_id')
            ->from('roles_users')
            ->join('roles', 'roles.id=roles_users.role_id')
            ->where('user_id', $id)
            ->get();

        if ($q->num_rows())
        {
            foreach ($q->result() as $r)
            {
                $role_users[$r->role_id] = $r->name;
            }
        }

        $this->data['auser'] = $obj;
        $this->data['roles'] = $roles;
        $this->data['role_users'] = $role_users;
        $this->display('acl/role_users');
    }

    public function toggle()
	{
        $role_id = (int) $this->input->get('role_id');
        $user_id = (int) $this->input->get('user_id');

        $role = $this->first_or_404('role_model', $role_id);
        $user = $this->first_or_404('user_model', $user_id);


        $response = "OK";

        $q = $this->db
            ->where('role_id', $role->id)
            ->where('user_id', $user->id)
            ->limit(1)
            ->get('roles_users');

        if ($q->num_rows())
        {
            $this->db
                ->where('role_id', $role->id)
                ->where('user_id', $user->id)
                ->limit(1)
                ->delete('roles_users');
            $response .= " OFF";
        }
        else
        {
            $this->db->insert('roles_users', [
                'role_id' => $role->id,
                'user_id' => $user->id
            ]);
            $response .= " ON";
        }

		http_response_code(200);
		echo $response;
	}

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->user->is_superuser)
        {
            redirect('dashboard');
        }
    }
}
