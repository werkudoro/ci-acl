<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Auth extends Base_controller
{
    /**
     * Validate
     *
     * @return void
     */
    protected function validate()
    {
        $this->load->model('user_model');
        $user = $this->user_model->getby_email($this->input->post('email'));

        if ($user)
        {
            if ($user->is_active)
            {
                if (password_verify($this->input->post('credential'), $user->credential))
                {
                    $this->session->set_userdata(self::SESS_USER, (int) $user->id);
                    if ($user->is_staff)
                    {
                        redirect('dashboard');
                    }

                    redirect('site');
                }
                else
                {
                    $this->set_message('Sandi salah', self::MSG_ERROR);
                }
            }
            else
            {
                $this->set_message('Akun tidak aktif', self::MSG_ERROR);
            }
        }
        else
        {
            $this->set_message('Email belum terdaftar', self::MSG_ERROR);
        }
    }

    /**
     * Auth
     *
     * @return void
     */
    public function index()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->input->method() === 'post')
        {
            $this->form_validation->set_rules([
                [
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required|max_length[128]|valid_email'
                ],
                [
                    'field' => 'credential',
                    'label' => 'Sandi',
                    'rules' => 'required|min_length[8]|max_length[128]'
                ]
            ]);
            if ($this->form_validation->run())
            {
                $this->validate();
            }
        }

        $this->display('acl/auth');
    }

    /**
     * Logout
     *
     * @return void
     */
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('acl/auth');
    }
}
