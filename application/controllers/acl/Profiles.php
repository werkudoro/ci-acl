<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Profiles extends App
{
    public function index()
    {
        $this->display('acl/profiles/index');
    }

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->user->is_superuser)
        {
            redirect('dashboard');
        }
    }
}
