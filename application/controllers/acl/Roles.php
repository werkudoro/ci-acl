<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Roles extends App_crud
{
    public function index()
    {
        $this->display('acl/roles/index');
    }

    /**
     * Data table rows
     *
     * @return void
     */
    public function rows()
    {
        extract($this->parse_datatable_params());

        $num_rows = $this->get_num_rows($keywords);

        $cols = ['name', 'readonly'];
        $col = isset($cols[$col_order]) ? $cols[$col_order] : 0;

        $q = $this->db
            ->select('id, name, readonly')
            ->like('name', $keywords)
            ->limit($length, $start)
            ->order_by($col, $dir_order)
            ->get('roles');

        $data = [];

        foreach ($q->result() as $row)
        {
            $data[] = [
                $row->name,
                $row->readonly ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>',
                $this->action_column($row, 'acl/roles/action_column')
            ];
        }

        $this->datatable_response($data, $num_rows, $num_rows, $draw);
    }

    /**
     *
     * @param  string $keywords
     * @return integer
     */
    protected function get_num_rows($keywords='')
    {
        $q = $this->db
            ->select('count(id) as num_rows')
            ->like('name', $keywords)
            ->get('roles');
        return $q->num_rows() ? ((int) $q->row()->num_rows) : 0;
    }

    public function create()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->method() === 'post')
        {
            $this->form_validation->set_rules([
                [
                    'field' => 'name',
                    'label' => 'Nama',
                    'rules' => 'required|max_length[128]|is_unique[roles.name]'
                ]
            ]);
            if ($this->form_validation->run() === true)
            {
                $this->load->model('role_model');
                $this->role_model->insert([
                    'name' => $this->input->post('name')
                ]);
                $this->flash('Record berhasil disimpan', self::MSG_SUCCESS);
                redirect('acl/roles');
            }
        }

        $this->display('acl/roles/create');
    }

    public function update()
    {
        $id = (int) $this->input->get('id');
        $obj = $this->first_or_404('role_model', $id);

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['record'] = $obj;

        if ($this->input->method() === 'post')
        {
            $this->form_validation->set_rules([
                [
                    'field' => 'name',
                    'label' => 'Nama',
                    'rules' => 'required|max_length[128]|callback__available_name'
                ]
            ]);
            if ($this->form_validation->run() === true)
            {
                $this->db->where('readonly', false);
                $this->role_model->update([
                    'name' => $this->input->post('name')
                ], $id);
                $this->flash('Record berhasil disimpan', self::MSG_SUCCESS);
                redirect('acl/roles');
            }
        }

        $this->display('acl/roles/update');
    }

    public function delete()
    {
        $id = (int) $this->input->get('id');
        $obj = $this->first_or_404('role_model', $id);
        $this->db->where('readonly', false);
        $this->role_model->delete($obj->id);
        $this->flash('Record berhasil dihapus', 'success');
        redirect('acl/roles');
    }

    public function _available_name($val)
    {
        $id = (int) $this->input->get('id');
        $q = $this->db->where('name', $val)->where('id !=', $id)->get('roles');
        if ($q->num_rows())
        {
            $this->form_validation->set_message('_available_name', 'Nama harus unik');
            return false;
        }

        return true;
    }

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->user->is_superuser)
        {
            redirect('dashboard');
        }
    }
}
