<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Permission_users extends App_crud
{
    public function index()
    {
        $id = (int) $this->input->get('id');
        $obj = $this->first_or_404('user_model', $id);
        $q = $this->db->get('permissions');
        $permissions = [];
        $permission_users = [];

        foreach ($q->result() as $r)
        {
            $permissions[$r->id] = $r->name;
        }

        $q = $this->db
            ->select('permissions_users.id, permission_id, permissions.name, user_id')
            ->from('permissions_users')
            ->join('permissions', 'permissions.id=permissions_users.permission_id')
            ->where('user_id', $id)
            ->get();

        if ($q->num_rows())
        {
            foreach ($q->result() as $r)
            {
                $permission_users[$r->permission_id] = $r->name;
            }
        }

        $this->data['auser'] = $obj;
        $this->data['permissions'] = $permissions;
        $this->data['permission_users'] = $permission_users;
        $this->display('acl/permission_users');
    }

    public function toggle()
	{
        $permission_id = (int) $this->input->get('permission_id');
        $user_id = (int) $this->input->get('user_id');

        $permission = $this->first_or_404('permission_model', $permission_id);
        $user = $this->first_or_404('user_model', $user_id);

        $response = "OK";

        $q = $this->db
            ->where('permission_id', $permission->id)
            ->where('user_id', $user->id)
            ->limit(1)
            ->get('permissions_users');

        if ($q->num_rows())
        {
            $this->db
                ->where('permission_id', $permission->id)
                ->where('user_id', $user->id)
                ->limit(1)
                ->delete('permissions_users');
            $response .= " OFF";
        }
        else
        {
            $this->db->insert('permissions_users', [
                'permission_id' => $permission->id,
                'user_id' => $user->id
            ]);
            $response .= " ON";
        }

		http_response_code(200);
		echo $response;
	}

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->user->is_superuser)
        {
            redirect('dashboard');
        }
    }
}
