<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Permission_roles extends App_crud
{
    public function index()
    {
        $id = (int) $this->input->get('id');
        $obj = $this->first_or_404('role_model', $id);
        $q = $this->db->get('permissions');
        $permissions = [];
        $role_permissions = [];

        foreach ($q->result() as $r)
        {
            $permissions[$r->id] = $r->name;
        }

        $q = $this->db
            ->select('permissions_roles.id, permission_id, permissions.name, role_id')
            ->from('permissions_roles')
            ->join('permissions', 'permissions.id=permissions_roles.permission_id')
            ->where('role_id', $id)
            ->get();

        if ($q->num_rows())
        {
            foreach ($q->result() as $r)
            {
                $role_permissions[$r->permission_id] = $r->name;
            }
        }

        $this->data['role'] = $obj;
        $this->data['permissions'] = $permissions;
        $this->data['role_permissions'] = $role_permissions;
        $this->display('acl/permission_roles');
    }

    public function toggle()
	{
        $permission_id = (int) $this->input->get('permission_id');
        $role_id = (int) $this->input->get('role_id');

        $permission = $this->first_or_404('permission_model', $permission_id);
        $role = $this->first_or_404('role_model', $role_id);

        $response = "OK";

        $q = $this->db
            ->where('permission_id', $permission->id)
            ->where('role_id', $role->id)
            ->limit(1)
            ->get('permissions_roles');

        if ($q->num_rows())
        {
            $this->db
                ->where('permission_id', $permission->id)
                ->where('role_id', $role->id)
                ->limit(1)
                ->delete('permissions_roles');
            $response .= " OFF";
        }
        else
        {
            $this->db->insert('permissions_roles', [
                'permission_id' => $permission->id,
                'role_id' => $role->id
            ]);
            $response .= " ON";
        }

		http_response_code(200);
		echo $response;
	}

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->user->is_superuser)
        {
            redirect('dashboard');
        }
    }
}
