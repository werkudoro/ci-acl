<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Users extends App_crud
{
    public function index()
    {
        $this->load->model('user_model');
        $this->data['records'] = $this->user_model->all();
        $this->display('acl/users/index');
    }

    /**
     * Data table rows
     *
     * @return void
     */
    public function rows()
    {
        extract($this->parse_datatable_params());

        $num_rows = $this->get_num_rows($keywords);

        $cols = ['name', 'email'];
        $col = isset($cols[$col_order]) ? $cols[$col_order] : 0;

        $q = $this->db
            ->select('id, name, email, is_active')
            ->distinct()
            ->like('name', $keywords)
            ->like('email', $keywords)
            ->limit($length, $start)
            ->order_by($col, $dir_order)
            ->get('users');

        $data = [];

        foreach ($q->result() as $row)
        {
            $data[] = [
                $row->name,
                '<i class="glyphicon glyphicon-envelope"></i> '.$row->email,
                $row->is_active ? '<i class="glyphicon glyphicon-check"></i>' : '<i class="glyphicon glyphicon-remove"></i>',
                $this->action_column($row, 'acl/users/action_column')
            ];
        }

        $this->datatable_response($data, $num_rows, $num_rows, $draw);
    }

    /**
     *
     * @param  string $keywords
     * @return integer
     */
    protected function get_num_rows($keywords='')
    {
        $q = $this->db
            ->select('count(id) as num_rows')
            ->distinct()
            ->like('name', $keywords)
            ->or_like('email', $keywords)
            ->get('users');
        return $q->num_rows() ? ((int) $q->row()->num_rows) : 0;
    }

    public function create()
    {
        $this->load->library('form_validation');
        $this->load->helper('form');

        if ($this->input->method() === 'post')
        {
            $this->form_validation->set_rules([
                [
                    'field' => 'name',
                    'label' => 'Nama',
                    'rules' => 'required|max_length[128]'
                ],
                [
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required|max_length[128]|is_unique[users.email]'
                ],
                [
                    'field' => 'credential',
                    'label' => 'Sandi',
                    'rules' => 'required|max_length[128]|min_length[8]'
                ]
            ]);
            if ($this->form_validation->run() === true)
            {
                $this->load->model('user_model');
                $this->user_model->insert([
                    'name' => ucwords($this->input->post('name')),
                    'email' => $this->input->post('email'),
                    'credential' => password_hash($this->input->post('credential'), PASSWORD_BCRYPT)
                ]);
                $this->flash('Record berhasil disimpan', self::MSG_SUCCESS);
                redirect('acl/users');
            }
        }

        $this->display('acl/users/create');
    }

    public function update()
    {
        $id = (int) $this->input->get('id');
        $obj = $this->first_or_404('user_model', $id);

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->data['record'] = $obj;

        if ($this->input->method() === 'post')
        {
            $rules = [
                [
                    'field' => 'name',
                    'label' => 'Nama',
                    'rules' => 'required|max_length[128]'
                ],
                [
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required|max_length[128]|callback__available_email'
                ]
            ];

            $credential = $this->input->post('credential');

            if ($credential && $obj->credential !== $credential)
            {
                $rules[] = [
                    'field' => 'credential',
                    'label' => 'Sandi',
                    'rules' => 'required|max_length[128]|min_length[8]'
                ];
            }

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === true)
            {
                $data = [
                    'name' => ucwords($this->input->post('name')),
                    'email' => $this->input->post('email')
                ];

                if ($credential && $obj->credential !== $credential)
                {
                    $data['credential'] = password_hash($this->input->post('credential'), PASSWORD_BCRYPT);
                }

                $this->user_model->update($data, $id);
                $this->flash('Record berhasil disimpan', self::MSG_SUCCESS);
                redirect('acl/users');
            }
        }

        $this->display('acl/users/update');
    }

    public function delete()
    {
        $id = (int) $this->input->get('id');
        $obj = $this->first_or_404('user_model', $id);
        $this->db->where('is_superuser', false);
        $this->user_model->delete($obj->id);
        $this->flash('Record berhasil dihapus', 'success');
        redirect('acl/users');
    }

    public function _available_email($val)
    {
        $id = (int) $this->input->get('id');
        $q = $this->db->where('email', $val)->where('id !=', $id)->get('users');
        if ($q->num_rows())
        {
            $this->form_validation->set_message('_available_email', 'Email harus unik');
            return false;
        }

        return true;
    }

    public function toggle_active()
	{
        $user_id = (int) $this->input->get('id');
        $user = $this->first_or_404('user_model', $user_id);

        $response = "OK";

        if ($user->is_active)
        {
            $this->db
                ->where('is_superuser', false)
                ->where('id', $user->id)
                ->limit(1)
                ->update('users', ['is_active' => false]);
            $response .= " OFF";
        }
        else
        {
            $this->db
                ->where('is_superuser', false)
                ->where('id', $user->id)
                ->limit(1)
                ->update('users', ['is_active' => true]);
            $response .= " ON";
        }

		http_response_code(200);
		echo $response;
	}

    public function toggle_staff()
	{
        $user_id = (int) $this->input->get('id');
        $user = $this->first_or_404('user_model', $user_id);

        $response = "OK";

        if ($user->is_staff)
        {
            $this->db
                ->where('is_superuser', false)
                ->where('id', $user->id)
                ->limit(1)
                ->update('users', ['is_staff' => false]);
            $response .= " OFF";
        }
        else
        {
            $this->db
                ->where('is_superuser', false)
                ->where('id', $user->id)
                ->limit(1)
                ->update('users', ['is_staff' => true]);
            $response .= " ON";
        }

		http_response_code(200);
		echo $response;
	}

    public function __construct()
    {
        parent::__construct();

        if ( ! $this->user->is_superuser)
        {
            redirect('dashboard');
        }
    }
}
