<?php

defined('BASEPATH') or exit('No direct script access allowed');


class Migration_Add_system extends CI_Migration
{
    public function up()
    {
        $sql = "create table if not exists roles (
                id int unsigned not null auto_increment,
                name varchar(128) not null unique,
                readonly boolean default false,
                created_at datetime default current_timestamp,
                updated_at datetime default current_timestamp on update current_timestamp,
                primary key(id)
            )engine=innodb";
        $this->db->query($sql);

        $sql = "create table if not exists users (
                id int unsigned not null auto_increment,
                email varchar(128) not null unique,
                name varchar(128) not null,
                credential varchar(128) not null,
                is_superuser boolean default false,
                is_staff boolean default false,
                is_active boolean default false,
                last_login datetime default null,
                created_at datetime default current_timestamp,
                updated_at datetime default current_timestamp on update current_timestamp,
                primary key(id)
            )engine=innodb";
        $this->db->query($sql);

        $sql = "create table if not exists roles_users (
                id int unsigned not null auto_increment,
                role_id int unsigned not null,
                    index(role_id),
                    foreign key(role_id) references roles(id),
                user_id int unsigned not null,
                    index(user_id),
                    foreign key(user_id) references users(id),
                created_at datetime default current_timestamp,
                updated_at datetime default current_timestamp on update current_timestamp,
                unique(role_id, user_id),
                primary key(id)
            )engine=innodb";
        $this->db->query($sql);

        $sql = "create table if not exists permissions (
                id int unsigned not null auto_increment,
                name varchar(128) not null unique,
                parent int unsigned default 0,
                readonly boolean default false,
                created_at datetime default current_timestamp,
                updated_at datetime default current_timestamp on update current_timestamp,
                primary key(id)
            )engine=innodb";
        $this->db->query($sql);

        $sql = "create table if not exists permissions_roles (
                id int unsigned not null auto_increment,
                permission_id int unsigned not null,
                    index(permission_id),
                    foreign key(permission_id) references permissions(id),
                role_id int unsigned not null,
                    index(role_id),
                    foreign key(role_id) references roles(id),
                created_at datetime default current_timestamp,
                updated_at datetime default current_timestamp on update current_timestamp,
                unique(permission_id, role_id),
                primary key(id)
            )engine=innodb";
        $this->db->query($sql);

        $sql = "create table if not exists permissions_users (
                id int unsigned not null auto_increment,
                permission_id int unsigned not null,
                    index(permission_id),
                    foreign key(permission_id) references permissions(id),
                user_id int unsigned not null,
                    index(user_id),
                    foreign key(user_id) references users(id),
                created_at datetime default current_timestamp,
                updated_at datetime default current_timestamp on update current_timestamp,
                unique(permission_id, user_id),
                primary key(id)
            )engine=innodb";
        $this->db->query($sql);

        $this->db->query("insert into roles (name, readonly)
            values ('admin', true),
            ('manager', true),
            ('staff', true)");

        $email = 'superuser@example.com';
        $password = password_hash('superuser@example.com', PASSWORD_BCRYPT);
        $this->db->query("insert into users (name, email, credential, is_superuser, is_staff, is_active)
            values('Super User', '$email', '$password', true, true, true)");

        $this->db->query("insert into permissions (name, readonly)
            values('manage_roles', true),
            ('manage_users', true),
            ('manage_permissions', true)");
    }

    public function down()
    {
        $this->db->query("drop table if exists permissions_users");
        $this->db->query("drop table if exists permissions_roles");
        $this->db->query("drop table if exists permissions");
        $this->db->query("drop table if exists roles_users");
        $this->db->query("drop table if exists users");
        $this->db->query("drop table if exists roles");
    }
}
