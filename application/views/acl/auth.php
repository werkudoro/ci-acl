<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Auth</title>
    <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/bootstrap-theme.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/auth/css/auth.css') ?>">
</head>
<body>


    <?= form_open('', ['id' => 'loginForm', 'class' => 'panel panel-default']) ?>

        <div class="panel-heading">
            Form Login
        </div>

        <div class="panel-body">

            <?php $this->load->view('dashboard/parts/message') ?>

            <div class="form-group <?= form_error('email') ? 'has-error' : '' ?>">
                <label for="email" class="control-label">Email</label>
                <input type="email" name="email" value="<?= set_value('email') ?>" class="form-control">
                <?= form_error('email', '<div class="help-block">', '</div>') ?>
            </div>

            <div class="form-group <?= form_error('credential') ? 'has-error' : '' ?>">
                <label for="credential" class="control-label">Sandi</label>
                <input type="password" name="credential" value="<?= set_value('credential') ?>" class="form-control">
                <?= form_error('credential', '<div class="help-block">', '</div>') ?>
            </div>
        </div>

        <div class="panel-footer">
            <button name="submit" type="submit" class="btn btn-primary">
                <i class="glyphicon glyphicon-send"></i> Masuk
            </button>
        </div>

    <?= form_close() ?>

    <script src="<?= base_url('assets/vendor/jquery/jquery.min.js') ?>" charset="utf-8"></script>
    <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.min.js') ?>" charset="utf-8"></script>
</body>
</html>
