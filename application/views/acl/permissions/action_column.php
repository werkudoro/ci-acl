<?php if ( ! $obj->readonly): ?>
    <a href="<?= site_url('acl/permissions/update').'?id='.$obj->id ?>" data-role="btn-edit" class="btn btn-xs btn-default"
        title="Edit">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
<?php endif ?>
<?php if ( ! $obj->readonly): ?>
    <a href="javascript:doDelete('<?= site_url('acl/permissions/delete').'?id='.$obj->id ?>');" data-role="btn-delete" class="btn btn-xs btn-danger"
        title="Hapus">
        <i class="glyphicon glyphicon-remove"></i>
    </a>
<?php endif; ?>
