<?php $this->load->view('dashboard/parts/header') ?>

<div class="row">
    <div class="col-xs-12">

        <div class="row">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li>Acl</li>
                    <li><a href="<?= site_url('acl/permissions') ?>">Hak Akses</a></li>
                    <li class="active">Index</li>
                </ul>
            </div>
            <div class="col-md-6 text-right">
                <a href="<?= site_url('acl/permissions/create') ?>" class="btn btn-success" title="Tambah Record">
                    <i class="glyphicon glyphicon-plus"></i>
                </a>
            </div>
        </div>

        <?php $this->load->view('dashboard/parts/message') ?>

        <table data-role="datatable" data-url="<?= site_url('acl/permissions/rows') ?>" class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th class="fit">Nama</th>
                    <th>Readonly</th>
                    <th class="fit">Aksi</th>
                </tr>
            </thead>
        </table>

    </div>
</div>

<?php $this->load->view('dashboard/parts/footer') ?>
