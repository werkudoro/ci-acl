<?php $this->load->view('dashboard/parts/header') ?>

<div class="row">
    <div class="col-xs-12">

        <div class="row">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li>Acl</li>
                    <li><a href="<?= site_url('acl/roles') ?>">Role</a></li>
                    <li class="active">Hak Akses Role</li>
                </ul>
            </div>
            <div class="col-md-6 text-right">
                <a href="<?= site_url('acl/roles') ?>" class="btn btn-default" title="Kembali">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </a>
            </div>
        </div>

        <p>
            Role: <b><?= $role->name ?></b>
        </p>

        <table class="table table-bordered table-striped">
            <tbody>
                <?php foreach ($permissions as $key => $val): ?>
                    <tr>
                        <td class="fit">
                            <input type="checkbox" name="role_permissions[]" value="<?= $key ?>"
                                data-role="ajax-checkbox"
                                data-url="<?= site_url('acl/permission_roles/toggle') ?>?permission_id=<?= $key ?>&role_id=<?= $role->id ?>"
                                <?= array_key_exists($key, $role_permissions) ? 'checked="checked"' : '' ?>>
                        </td>
                        <td><?= $val ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>

<?php $this->load->view('dashboard/parts/footer') ?>
