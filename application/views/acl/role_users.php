<?php $this->load->view('dashboard/parts/header') ?>

<div class="row">
    <div class="col-xs-12">

        <div class="row">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li>Acl</li>
                    <li><a href="<?= site_url('acl/users') ?>">User</a></li>
                    <li class="active">Role</li>
                </ul>
            </div>
            <div class="col-md-6 text-right">
                <a href="<?= site_url('acl/users') ?>" class="btn btn-default" title="Kembali">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </a>
            </div>
        </div>

        <p>
            User: <b><?= $auser->name ?></b>
            <i class="glyphicon glyphicon-envelope"></i> <?= $auser->email ?>
        </p>

        <table class="table table-bordered table-striped">
            <tbody>
                <?php foreach ($roles as $key => $val): ?>
                    <tr>
                        <td class="fit">
                            <input type="checkbox" name="role_users[]" value="<?= $key ?>"
                                data-role="ajax-checkbox"
                                data-url="<?= site_url('acl/role_users/toggle') ?>?role_id=<?= $key ?>&user_id=<?= $auser->id ?>"
                                <?= array_key_exists($key, $role_users) ? 'checked="checked"' : '' ?>>
                        </td>
                        <td><?= $val ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>

<?php $this->load->view('dashboard/parts/footer') ?>
