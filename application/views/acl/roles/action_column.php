<?php if ( ! $obj->readonly): ?>
    <a href="<?= site_url('acl/roles/update').'?id='.$obj->id ?>" data-role="btn-edit" class="btn btn-xs btn-default"
        title="Edit">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
<?php endif ?>
<a href="<?= site_url('acl/permission_roles').'?id='.$obj->id ?>" data-role="btn-role" class="btn btn-xs btn-default"
    title="Hak Akses">
    <i class="glyphicon glyphicon-list"></i>
</a>
<?php if ( ! $obj->readonly): ?>
    <a href="javascript:doDelete('<?= site_url('acl/roles/delete').'?id='.$obj->id ?>');" data-role="btn-delete" class="btn btn-xs btn-danger"
        title="Hapus">
        <i class="glyphicon glyphicon-remove"></i>
    </a>
<?php endif; ?>
