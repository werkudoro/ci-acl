<a href="<?= site_url('acl/users/update').'?id='.$obj->id ?>" data-role="btn-edit" class="btn btn-xs btn-default"
    title="Edit">
    <i class="glyphicon glyphicon-edit"></i>
</a>
<a href="<?= site_url('acl/role_users').'?id='.$obj->id ?>" data-role="btn-role" class="btn btn-xs btn-default"
    title="Role">
    <i class="glyphicon glyphicon-user"></i>
</a>
<a href="<?= site_url('acl/permission_users').'?id='.$obj->id ?>" data-role="btn-role" class="btn btn-xs btn-default"
    title="Hak Akses">
    <i class="glyphicon glyphicon-list"></i>
</a>
<a href="javascript:doDelete('<?= site_url('acl/users/delete').'?id='.$obj->id ?>');" data-role="btn-delete" class="btn btn-xs btn-danger"
    title="Hapus">
    <i class="glyphicon glyphicon-remove"></i>
</a>
