<?php $this->load->view('dashboard/parts/header') ?>

<div class="row">
    <div class="col-xs-12">

        <div class="row">
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li>Acl</li>
                    <li><a href="<?= site_url('acl/users') ?>">Akun</a></li>
                    <li class="active">Create</li>
                </ul>
            </div>
            <div class="col-md-6 text-right">
                <a href="<?= site_url('acl/users') ?>" class="btn btn-default" title="Kembali">
                    <i class="glyphicon glyphicon-chevron-left"></i>
                </a>
            </div>
        </div>

        <?php $this->load->view('dashboard/parts/message') ?>

        <?= form_open(site_url('acl/users/create'), ['class' => 'panel panel-default']) ?>
            <div class="panel-body">
                <div class="form-group <?= form_error('name') ? 'has-error' : '' ?>">
                    <label for="name" class="control-label">Nama</label>
                    <input type="text" name="name" value="<?= set_value('name') ?>" class="form-control">
                    <?= form_error('name', '<div class="help-block">', '</div>') ?>
                </div>

                <div class="form-group <?= form_error('email') ? 'has-error' : '' ?>">
                    <label for="email" class="control-label">Email</label>
                    <input type="email" name="email" value="<?= set_value('email') ?>" class="form-control">
                    <?= form_error('email', '<div class="help-block">', '</div>') ?>
                </div>

                <div class="form-group <?= form_error('credential') ? 'has-error' : '' ?>">
                    <label for="credential" class="control-label">Sandi</label>
                    <input type="text" name="credential" value="<?= set_value('credential') ?>" class="form-control">
                    <?= form_error('credential', '<div class="help-block">', '</div>') ?>
                </div>
            </div>
            <div class="panel-footer">
                <button name="submit" type="submit" class="btn btn-primary">
                    <i class="glyphicon glyphicon-save"></i> Simpan
                </button>
            </div>
        <?= form_close() ?>

    </div>
</div>

<?php $this->load->view('dashboard/parts/footer') ?>
