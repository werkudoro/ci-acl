<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Auth</title>
    <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/bootstrap-theme.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/vendor/DataTables/datatables.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dashboard/css/dashboard.css') ?>">
</head>
<body>
    <?php $this->load->view('dashboard/parts/navbar') ?>

    <div class="container-fluid">
