<?php if (isset($message) && $message): ?>

    <div class="alert alert-<?= $message->type === 'error' ? 'danger' : $message->type ?>" style="margin:1rem 0; display: block;">
        <?php if (is_array($message->message)): ?>
            <ul>
                <?php foreach ($message->$message as $m): ?>
                    <li><?= $m ?></li>
                <?php endforeach ?>
            </ul>
        <?php else: ?>
            <?= $message->message ?>
        <?php endif ?>
    </div>

<?php endif ?>
